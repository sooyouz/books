const path = require('path');
const { ESLint } = require('eslint');

module.exports = [
  {
    files: ['**/*.ts', '**/*.tsx'],
    ignores: ['node_modules/**'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
      parser: require('@typescript-eslint/parser'), // Ensure the parser is correctly imported and used
      parserOptions: {
        project: path.resolve(__dirname, './tsconfig.json'), // Use path.resolve for correct path resolution
        ecmaFeatures: {
          jsx: true,
        },
      },
    },
    plugins: {
      '@typescript-eslint': require('@typescript-eslint/eslint-plugin'),
      react: require('eslint-plugin-react'),
    },
    settings: {
      react: {
        version: 'detect',
      },
    },
    rules: {
      'max-len': ['warn', { code: 100 }],
      'brace-style': ['error', '1tbs', { allowSingleLine: false }],
      curly: 'error',
      'no-var': 'warn',
      'no-restricted-syntax': [
        'warn',
        {
          selector: ':matches(ImportNamespaceSpecifier, ExportAllDeclaration, ExportNamespaceSpecifier)',
          message: 'Import/Export only modules you need.',
        },
      ],
      '@typescript-eslint/naming-convention': [
        'error',
        {
            selector: [
              "variableLike",
              "property",
              "method",
              "parameterProperty",
              "accessor"
            ], format: ["camelCase"]
          },
          {
            selector: [
              "typeLike"
            ], format: ["PascalCase"]
          },
          {
            // This selector is for functions and components built by functions
            selector: [
              "function"
            ], format: ["camelCase", "PascalCase"]
          },
          {
            selector: [
              "enumMember"
            ], format: ["UPPER_CASE"]
          },
          {
            selector: "variable",
            modifiers: ["const"],
            format: ["UPPER_CASE", "camelCase"]
          },
          {
            selector: 'objectLiteralProperty',
            format: null, // Disable naming convention check for object literal properties
          },
      ],
      '@typescript-eslint/no-empty-interface': 'warn',

      // React specific rules
      'react/no-multi-comp': ['error', { ignoreStateless: true }],
      'react/jsx-filename-extension': ['error', { extensions: ['.tsx'] }],
      'react/sort-comp': [
        'error',
        {
          order: [
            'static-variables',
            'static-methods',
            'lifecycle',
            'everything-else',
            'render',
          ],
          groups: {
            lifecycle: [
              'constructor',
              'componentDidMount',
              'componentWillUpdate',
              'componentWillUnmount',
              'render',
            ],
          },
        },
      ],
      'react/jsx-tag-spacing': [
        'error',
        {
          closingSlash: 'never',
          beforeSelfClosing: 'always',
          afterOpening: 'never',
          beforeClosing: 'never',
        },
      ],
      'react/jsx-wrap-multilines': [
        'error',
        {
          declaration: 'parens',
          assignment: 'parens',
          return: 'parens-new-line',
          arrow: 'parens-new-line',
          condition: 'ignore',
          logical: 'ignore',
          prop: 'ignore',
        },
      ],
    },
  },
];
